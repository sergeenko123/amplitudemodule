package com.kitegroup.amplitudemodule.analytics

data class AnalyticsBucket(
  val eventName: String,
  val parameters: Map<String, Any> = mutableMapOf(),
)
