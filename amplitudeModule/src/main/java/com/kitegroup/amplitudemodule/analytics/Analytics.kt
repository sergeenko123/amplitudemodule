package com.kitegroup.amplitudemodule.analytics

import org.json.JSONObject

interface Analytics {

  fun track(event: String, parameters: Map<String, Any>? = null)

  fun track(event: AnalyticsEvent)
}
