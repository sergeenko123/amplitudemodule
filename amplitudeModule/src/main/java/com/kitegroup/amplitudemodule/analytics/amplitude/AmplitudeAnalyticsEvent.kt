package com.kitegroup.amplitudemodule.analytics.amplitude

import com.kitegroup.amplitudemodule.analytics.AnalyticsBucket
import com.kitegroup.amplitudemodule.analytics.AnalyticsEvent

interface AmplitudeAnalyticsEvent : AnalyticsEvent {

  fun getAmplitudeBucket(): AnalyticsBucket
}
