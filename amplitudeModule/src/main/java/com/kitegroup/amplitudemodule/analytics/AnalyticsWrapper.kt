package com.kitegroup.amplitudemodule.analytics

import android.util.Log
import android.util.Log.d
import androidx.annotation.Size
import com.amplitude.BuildConfig
abstract class AnalyticsWrapper(
  private val isEnabled: Boolean,
) {

  abstract fun trackEvent(
    @Size(min = 1L, max = 40L) eventName: String,
    parameters: Map<String, Any>? = null,
  )

  /**
   * Track event if current analytic implementation is enabled & buildConfig not debug
   */
  protected fun trackIfEnabled(
    eventName: String,
    event: () -> Unit,
  ) {
    if (BuildConfig.DEBUG || !isEnabled) {
      Log.d("AMPLITUDE","Track Analytic Event - $eventName")
      return
    }

    event.invoke()
  }
}
