package com.kitegroup.amplitudemodule.analytics.amplitude

import com.kitegroup.amplitudemodule.analytics.AnalyticsDispatcher

class AmplitudeAnalyticsDispatcher(
  private val wrapper: AmplitudeAnalyticsWrapper,
) : AnalyticsDispatcher<AmplitudeAnalyticsEvent> {

  override fun track(e: AmplitudeAnalyticsEvent) {
    val bucket = e.getAmplitudeBucket()
    wrapper.trackEvent(bucket.eventName, bucket.parameters)
  }

  override fun track(event: String, parameters: Map<String, Any>?) {
    wrapper.trackEvent(event, parameters)
  }
}
