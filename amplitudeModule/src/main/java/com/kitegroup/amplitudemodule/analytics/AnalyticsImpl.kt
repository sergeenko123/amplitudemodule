package com.kitegroup.amplitudemodule.analytics

import android.app.Application
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.util.Log
import com.amplitude.BuildConfig
import com.amplitude.api.Amplitude
import com.kitegroup.amplitudemodule.analytics.amplitude.AmplitudeAnalyticsDispatcher
import com.kitegroup.amplitudemodule.analytics.amplitude.AmplitudeAnalyticsEvent
import com.kitegroup.amplitudemodule.analytics.amplitude.AmplitudeAnalyticsWrapper
import org.json.JSONObject

class AnalyticsImpl(
  application: Application,
  amplitudeKey: String,
  isEnabled: Boolean = true
) : Analytics {

  private val amplitudeAnalyticsDispatcher: AmplitudeAnalyticsDispatcher
  private val FIRST_LAUNCH = "FIRST_LAUNCH"

  init {
    val client =  Amplitude.getInstance()
      .initialize(application, amplitudeKey)
      .enableForegroundTracking(application)
    val amplitudeAnalyticsWrapper = AmplitudeAnalyticsWrapper(
      isEnabled = isEnabled,
      amplitudeClient = client
    )
    amplitudeAnalyticsDispatcher = AmplitudeAnalyticsDispatcher(amplitudeAnalyticsWrapper)
    trackFirstLaunch(application)
    track("Session Start")
  }

  private fun trackFirstLaunch(application: Application) {
    val preferences = application.getSharedPreferences(application.packageName, MODE_PRIVATE)
    if(preferences.getBoolean(FIRST_LAUNCH, true)){
      track("First Open")
      preferences.edit().putBoolean(FIRST_LAUNCH, false).apply()
    }
  }

  override fun track(event: String, parameters: Map<String, Any>?) {
    amplitudeAnalyticsDispatcher.track(event, parameters)
    Log.i("ANALYTIC_TRACK", String.format("%s %s", event, parameters.toString()))
  }


  override fun track(event: AnalyticsEvent) {
    if (event is AmplitudeAnalyticsEvent) {
      amplitudeAnalyticsDispatcher.track(event)
      if(BuildConfig.DEBUG) {
        Log.i("ANALYTIC_TRACK", String.format("%s %s", event.getAmplitudeBucket().eventName, event.getAmplitudeBucket().parameters.toString()))
      }
    }
  }
}
