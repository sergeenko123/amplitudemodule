package com.kitegroup.amplitudemodule.analytics

interface AnalyticsDispatcher<T : AnalyticsEvent> {

  fun track(event: String, parameters: Map<String, Any>? = null)

  fun track(e: T)
}
