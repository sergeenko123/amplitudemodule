package com.kitegroup.amplitudemodule.analytics.amplitude

import com.amplitude.api.AmplitudeClient
import com.kitegroup.amplitudemodule.analytics.AnalyticsWrapper
import org.json.JSONObject

class AmplitudeAnalyticsWrapper(
  isEnabled: Boolean,
  private val amplitudeClient: AmplitudeClient,
) : AnalyticsWrapper(isEnabled) {

  override fun trackEvent(eventName: String, parameters: Map<String, Any>?) {
    //trackIfEnabled(eventName) {
      if (parameters == null) {
        amplitudeClient.logEvent(eventName)
      } else {
        amplitudeClient.logEvent(eventName, JSONObject(parameters))
      }
    //}
  }
}
