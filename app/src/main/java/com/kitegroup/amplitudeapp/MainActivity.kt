package com.kitegroup.amplitudeapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.kitegroup.amplitudemodule.analytics.AnalyticsImpl

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AnalyticsImpl(application, "", true)
    }
}